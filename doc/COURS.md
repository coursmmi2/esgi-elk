# Requests

## PUT
```
PUT /my_blogs/_doc/1
{
  "title": "Fighting Ebola with Elastic",
  "category": "User Stories",
  "author": {
  "first_name": "Emily",
  "last_name": "Mother”
  }
}
```
## DELETE
```
DELETE /my_blogs/_doc/1
```

## GET

GET my_blogs/_doc/1

## MATCH phrase


## Slop
Flexibilité dans  une phrase
slop = 1 (un caractère/mot de différence)

# Exercice 1
```
PUT /my_blogs/_doc/1
{
    "title": "Better query execution",
    "category": "Engineering",
    "date": "2015-15-06T12:10:30Z",
    "autor_first_name": "Adrien",
    "author_last_name": "Grand",
    "author_company": "Elastic"
}

PUT /my_blogs/_doc/2
{
    "title": "The Story of Sense",
    "category": "Engineering",
    "date": "2015-28-06T12:10:30Z",
    "autor_first_name": "Boaz",
    "author_last_name": "Leskes",
    "author_company": ""
}

```


