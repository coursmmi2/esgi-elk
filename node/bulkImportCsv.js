var fs = require('fs');
var http = require('http');
const shellExec = require('shell-exec');
let csvToJson = require('csvtojson');
const converter = csvToJson({
  trim:true,
  delimiter: ","
})
var csvPath = `${__dirname}/la_data.csv`;
var jsonPath = `${__dirname}/data.json`;

async function test() {
  let jsonArray = await converter.
    fromFile(csvPath);
  let jsonString = jsonArray.map((e, index) => {
    // console.log(index)
    e.date = new Date(e.date).toISOString();
    return `{"index": { "_id":"-${index}", "_index": "my_blogs", "_type": "_doc" }}
    ${JSON.stringify(e)}
    `;
  }).join("");
  //console.log("jsonArray", jsonArray);
  //console.log("jsonString", jsonString);

  fs.appendFile('data.json', '\n', function (err) {
    if (err) throw err;
    console.log('json Saved!');
  });
  fs.writeFileSync('data.json', jsonString);
    // Async operation on the json
    // don't forget to call resolve and reject

  shellExec('curl --user elastic:changeme -H "Content-Type: application/json" -XPOST "localhost:9200/_bulk?pretty&refresh" --data-binary "@data.json"')
    .then(console.log)
    .catch(console.log);

}

test();

