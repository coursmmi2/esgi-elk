# Final document
open rendu.pdf in doc folder

# Dependencies
* Docker
* Node
* Npm

## Run docker
```
docker-compose up
```

## Run npm
In another shell run 

```
  npm install
```

## Run data import

move to folder /node and run bulkImportCsv

```
  cd node
  node bulkImportCsv.js
```

# Congratulation !

Open browser in :

* http://localhost:9200 for elasticsearch

* http://localhost:5601 for kibana

